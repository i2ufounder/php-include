var fs = require('fs');
var gutil = require('gulp-util');
var path = require('path');
var through = require('through2');
var Vinyl = require('vinyl');

var PluginError = gutil.PluginError;

var options;


function phpInclude (opts) {
    options = opts;

    if (options == null || typeof(options.includePath) != 'string') {
       throw new PluginError('php-include', 'You must supply the path for the includePath option in php-include!');
    }

    if ( typeof(options.garbage) == undefined ) {
        options.garbage = [];
    }

    if ( typeof(options.garbage) == 'array' ) {
        throw new PluginError('php-include', 'Garbage option must be an array.');
    }

    var stream = through.obj(function (file, enc, callback) {
        self = this;
        if (!file.isBuffer()) {
            this.emit('error', new PluginError('php-include', 'Streams not supported in php-include!'));
            return callback();
        }

        var fileOut = replace(file);

        this.push(fileOut);
        callback();
    });
    return stream;

}




function replace (fileIn) {
    var regex = /(?:include [^;]*?['"](.+?)['"].*?;)/ig;

    var newContents = fileIn.contents.toString();

    var match;
    while ( (match = regex.exec(fileIn.contents)) !== null ) {

        var fileIncludePath = path.join (options.includePath,  match[1]);
        try {
            var newFile = fs.readFileSync (fileIncludePath);
            var matchedFile = replace ( new Vinyl ({
                    path: fileIncludePath,
                    contents: new Buffer(newFile.toString())
                    })
                );

            matchedContent = removeTags(matchedFile.contents.toString());
            newContents = newContents.replace(match[0], matchedContent);

            if ( !options.garbage.contains ( fileIncludePath ) ) {
                options.garbage.push ( fileIncludePath );
            }
        }
        catch (exception) {
            var msg = 'Include not found in file: ' + fileIn.path + ' \n' + exception.message;
            self.emit('error', new PluginError('php-include', msg));
        }
    }

    fileIn.contents = new Buffer (newContents);

    return fileIn;
}


function removeTags (string) {

    var beginningPHP = /^(<\?php|<\?)\s{0,1}/ig;
    string = string.replace (beginningPHP, '');

    var endingPHP = /( {0,1}\?>)\s*$/ig;
    string = string.replace (endingPHP, '');

    return string;
}


Array.prototype.contains = function(obj) {
    var i = this.length;
    while (i--) {
        if (this[i] === obj) {
            return true;
        }
    }
    return false;
}



module.exports = phpInclude;
